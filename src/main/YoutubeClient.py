import json
import logging
from settings import *
from tools.requester import safely_get
from tools.connection_tools import *


class YoutubeClient:

    def __init__(self, api_key=YOUTUBE_API_KEY):
        self.api_key = YOUTUBE_API_KEY

    def get_channelId_from_user_name(self, user_name):
        URL = "https://www.googleapis.com/youtube/v3/channels?key={key}&forUsername={user_name}&part=id"
        url = URL.format(key=self.api_key, user_name=user_name)
        response = safely_get(url)
        return response['items'][0]['id']


    def get_all_comments(self, channelId):
        URL = "https://www.googleapis.com/youtube/v3/commentThreads?key={key}0&part=id,snippet" \
              "&allThreadsRelatedToChannelId={channelId}"
        url = URL.format(key=self.api_key, user_name=channelId)
        # pagination, es un todo


    def get_all_videos(self, channelId):
        URL = ""
