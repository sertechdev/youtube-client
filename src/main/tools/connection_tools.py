from pymongo import MongoClient


def get_mongo_connection(credentials):
    connection_string = "mongodb://{user}:{pass}@{host}:{port}/{authdb}".format(**credentials)
    client = MongoClient(connection_string, connect=False)

    return client
