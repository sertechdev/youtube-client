import logging
import json
from requests import get
from settings import GET_RETRIES


def safely_get(url):
    logging.info('Getting url {url}'.format(url=url))

    retries = 0
    while True:
        try:
            response = get(url)
            break
        except:
            retries += 1
            logging.exception('Exception while getting info from {url}'.format(url=url))
            if retries >= GET_RETRIES:
                return []
    logging.info('Response got {response} from url {url}'.format(response=response, url=url))

    response = json.loads(response)

    return response